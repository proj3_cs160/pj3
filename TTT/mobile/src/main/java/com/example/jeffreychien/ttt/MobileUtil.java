package com.example.jeffreychien.ttt;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jeffreychien on 11/24/15.
 */
public class MobileUtil {
    public static void sendMsg(NodeApi.GetConnectedNodesResult nodes, GoogleApiClient googleApiClient, String servicePath, String msg) {
        if (googleApiClient == null) { return; }
        if (nodes == null) {
            nodes = Wearable.NodeApi.getConnectedNodes(googleApiClient).await();
        }
        for( Node node: nodes.getNodes()) {
            Wearable.MessageApi.sendMessage(
                    googleApiClient, node.getId(),
                    servicePath, msg.getBytes(Charset.forName("UTF-8"))
            ).await();
        }
    }
    //return min lat, max lat, min long, max long
    //query will be cur_lat >= min_lat AND cur_lat <= max_lat AND cur_long >= min_long AND cur_long <= max_long
    public static List<Float> getBoundingBox(float curLat, float curLong, int distanceAway) {
        List<Float> result = new ArrayList<>();
        result.add(curLat - distanceAway);
        result.add(curLat + distanceAway);
        result.add(curLong - distanceAway);
        result.add(curLong + distanceAway);
        return result;
    }
}

