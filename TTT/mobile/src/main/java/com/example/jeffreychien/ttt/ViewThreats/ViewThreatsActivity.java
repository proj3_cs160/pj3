package com.example.jeffreychien.ttt.ViewThreats;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.jeffreychien.ttt.R;
import com.example.jeffreychien.ttt.ViewThreats.Fragments.ThreatListViewFragment;
import com.example.jeffreychien.ttt.ViewThreats.Fragments.ThreatMapViewFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by jeffreychien on 11/18/15.
 */
public class ViewThreatsActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener{
    double latitude = 37.8717, longitude = 122.2728;
    GoogleApiClient mGoogleApiClient;
    boolean connected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_threat);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!connected) {
            return true;
        }
        // Handle item selection
        Fragment fr;
        FragmentManager fm;
        FragmentTransaction fragmentTransaction;
        switch (item.getItemId()) {
            case R.id.list_view:
                fr = new ThreatListViewFragment();
                fr.setArguments(genLocationBundle());
                fm = getFragmentManager();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, fr);
                fragmentTransaction.commit();
                return true;
            case R.id.map_view:
                fr = new ThreatMapViewFragment();
                fr.setArguments(genLocationBundle());
                fm = getFragmentManager();
                fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, fr);
                fragmentTransaction.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
    private Bundle genLocationBundle() {
        Bundle result = new Bundle();
        result.putDouble("latitude", latitude);
        result.putDouble("longitude", longitude);
        return result;
    }

    @Override
    public void onConnected(Bundle bundle) {
            updateLocation();
            Fragment fr = new ThreatListViewFragment();
            fr.setArguments(genLocationBundle());
            FragmentManager fm = getFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.add(R.id.fragment_place, fr);
            fragmentTransaction.commit();
            connected = true;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void updateLocation() {
        Location lastLocation =  LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (lastLocation != null) {
            latitude = lastLocation.getLatitude();
            longitude = lastLocation.getLongitude();
        } else {
            latitude = 37.8717;
            longitude = 122.2728;
        }
    }
}
