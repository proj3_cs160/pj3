package com.example.jeffreychien.ttt.DeadmansSwitch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.jeffreychien.ttt.R;
import com.example.jeffreychien.ttt.WearUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by jeffreychien on 11/18/15.
 */
public class DeadmanSwitchMenuActivity extends Activity{
    Button holdButton;
    boolean shouldCallPolice = false;
    GoogleApiClient mGoogleApiClient;
    private Lock lock = new ReentrantLock();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.deadmans_switch_main);
        holdButton = (Button) findViewById(R.id.hold);
        holdButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    new Thread() {
                        @Override
                        public void run() {
                            WearUtil.sendMsg(null, mGoogleApiClient, "/launch-deadmans-switch", "wearmsg");
                        }
                    }.start();
                }
                return true;
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
    }

}
