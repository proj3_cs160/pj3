package com.example.jeffreychien.ttt;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.Toast;

import java.util.Date;

public class MyPostNotificationReceiver extends BroadcastReceiver {
    public static final String CONTENT_KEY = "contentText";

    public MyPostNotificationReceiver() {
    }


    // Copied from Threat.java in the mobile module
    public static String severityToColorHex(String severity) {
        switch(severity) {
            case "Low":
                return "#ffdd00"; // Yellow
            case "Medium":
                return "#fbb034"; // Orange
            case "High":
            default:
                return "#ff4c4c"; // Red
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();

        Intent threatDetails = new Intent(context, GetThreatDetailsService.class);
        threatDetails.setAction(Long.toString(System.currentTimeMillis()));
        threatDetails.putExtra("objectId", extras.getString("objectId"));
        PendingIntent detailIntent = PendingIntent.getService(context, 0, threatDetails, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent verifyThreat = new Intent(context, VerifyThreatService.class);
        verifyThreat.setAction(Long.toString(System.currentTimeMillis()));
        verifyThreat.putExtra("objectId", extras.getString("objectId"));
        PendingIntent verifyIntent = PendingIntent.getService(context, 0, verifyThreat, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.BigTextStyle bigStyle = new NotificationCompat.BigTextStyle();
        bigStyle.bigText(
            extras.getString("distance") + "\n" +
            extras.getString("time") + "\n" +
            extras.getString("description"));

        //set color here depending on threat level
        String severity = intent.getExtras().getString("severity");
        Bitmap bitmap = Bitmap.createBitmap(320, 320, Bitmap.Config.ARGB_8888);
        bitmap.eraseColor(Color.parseColor(severityToColorHex(severity)));

        //Notification notification = new Notification.Builder(context)
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.shield_logo)
                .setContentTitle(extras.getString("category"))
                .addAction(R.drawable.common_full_open_on_phone, "Get More Information", detailIntent)
                .addAction(R.drawable.ic_check_white_48dp, "Verify", verifyIntent)
                .setStyle(bigStyle);
        NotificationCompat.WearableExtender builderExtended = new NotificationCompat.WearableExtender();
        builderExtended.setBackground(bitmap);
        builder.extend(builderExtended);
        Notification notification = builder.build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        //randomize notification id
        long time = new Date().getTime();
        String tmpStr = String.valueOf(time);
        String last4Str = tmpStr.substring(tmpStr.length() - 5);
        int notificationId = Integer.valueOf(last4Str);
        notificationManager.notify(notificationId, notification);

        //Toast.makeText(context, context.getString(R.string.notification_posted), Toast.LENGTH_SHORT).show();
    }
}
