package com.example.jeffreychien.ttt;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by jeffreychien on 11/26/15.
 */
public class GetThreatDetailsService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        final GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
        Log.d("test", "Watch sending " + intent.getExtras().getString("msg"));
        new Thread() {
            @Override
            public void run() {
                WearUtil.sendMsg(null, mGoogleApiClient, "/threat-details", intent.getExtras().getString("objectId"));
            }
        }.start();
        stopSelf();
        return START_NOT_STICKY;
    }
}
