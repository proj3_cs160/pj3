package com.example.jeffreychien.ttt;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class DeadmansSwitchConfirmActivity extends Activity {
    Button confirmButton;
    TextView timeleftTextView;
    EditText editText;
    final static long INTERVAL=1000;
    long elapsed = 10000 + 1000;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deadmans_switch_confirm);
        timeleftTextView = (TextView) findViewById(R.id.timeleft);
        editText = (EditText) findViewById(R.id.editText);
        final TimerTask task = new TimerTask() {
            @Override
            public void run() {
                elapsed -= INTERVAL;
                if(elapsed <= 0) {
                    displayText("finished");
                    try {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + "8434721621"));
                        startActivity(callIntent);
                        cancel();
                        finish();
                        return;
                    } catch(SecurityException e) {}
                }
                displayText(elapsed / 1000 + " seconds");
            }
        };
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(task, 0, INTERVAL);
        confirmButton = (Button) findViewById(R.id.confirm);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String pwInput = editText.getText().toString();
                if(pwInput != null && pwInput.equals("1234")) {
                    timer.cancel();
                    finish();
                }
                else {
                    editText.setText("");
                }
            }
        });
    }

    private void displayText(final String text){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timeleftTextView.setText(text);
            }});
    }
}
