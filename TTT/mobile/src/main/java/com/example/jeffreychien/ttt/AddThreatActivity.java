package com.example.jeffreychien.ttt;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.jeffreychien.ttt.Threats.Threat;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.Wearable;
import com.parse.ParseException;
import com.parse.SaveCallback;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by jeffreychien on 11/18/15.
 */
public class AddThreatActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    Spinner categoryChooser, severityChooser;
    Button discardButton;
    double latitude = 37.8717, longitude = 122.2728;
    GoogleApiClient mGoogleApiClient;

    @Bind(R.id.incidentEditText) EditText incidentDescription;
    @Bind(R.id.suspectEditText) EditText suspectDescription;

    @Bind(R.id.submitButton) Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_threat);
        ButterKnife.bind(this);

        categoryChooser = (Spinner) findViewById(R.id.categoryChooser);
        severityChooser = (Spinner) findViewById(R.id.severityChooser);
        ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter.createFromResource(this,
                R.array.categories, android.R.layout.simple_spinner_item);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categoryChooser.setAdapter(categoryAdapter);

        final ArrayAdapter<CharSequence> severityAdapter = ArrayAdapter.createFromResource(this,
                R.array.severities, android.R.layout.simple_spinner_item);
        severityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        severityChooser.setAdapter(severityAdapter);
        discardButton = (Button) findViewById(R.id.discard);
        discardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    // Could be used for callback results
    private static int GPS_REQUEST_CODE = 1;

    /**
     * @return latitude and longitude, from GPS
     */
    public static Location getLocation(Activity activity) {
        // Check if we have the proper permission to use GPS
        String locationPermission = "android.permission.ACCESS_FINE_LOCATION";
        if (activity.checkCallingOrSelfPermission(locationPermission) == PackageManager.PERMISSION_DENIED) {
            // Ask for permission if we don't have it yet
            ActivityCompat.requestPermissions(activity, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, GPS_REQUEST_CODE);
            return null;
        } else {
            // Return the location
            LocationManager lm = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
            return lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
    }

    // Save threat to Parse on submit
    @OnClick(R.id.submitButton)
    public void submit() {

        Location location = getLocation(this);
        if (location == null) {
            location = new Location("");
            location.setLatitude(latitude);
            location.setLongitude(longitude);
        }
        // Save the various Threat attributes
        Threat threat = new Threat();
        threat.setLocation(location);
        threat.setSeverity(severityChooser.getSelectedItem().toString());
        threat.setCategory(categoryChooser.getSelectedItem().toString());
        threat.setIncidentDescription(incidentDescription.getText().toString());
        threat.setSuspectDesciption(suspectDescription.getText().toString());
        threat.saveInBackground(new SaveCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    Toast.makeText(AddThreatActivity.this, "Threat reported!", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(AddThreatActivity.this, "Error saving threat.", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    @Override
    public void onConnected(Bundle bundle) {
        updateLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    private void updateLocation() {
        Location lastLocation =  LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (lastLocation != null) {
            latitude = lastLocation.getLatitude();
            longitude = lastLocation.getLongitude();
        } else {
            latitude = 37.8717;
            longitude = 122.2728;
        }
    }
}
