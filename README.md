# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository holds the code for our project, Triple Threat Tracker. Triple Threat Tracker is a crowd-sourced app that helps users report and find out about recent crimes in a neighborhood so that they can warn others as well as stay safe themselves. 

### How do I get set up? ###

Simply download the files and run using a smartwatch/smartphone emulator. 

### Who do I talk to? ###
Owners of this project include:
Shirley Trinh

Jack Chen

Jeffrey Chien

Joeson Chiang

Austin Chen
