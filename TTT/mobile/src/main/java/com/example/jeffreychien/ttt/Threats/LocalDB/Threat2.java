package com.example.jeffreychien.ttt.Threats.LocalDB;

import java.security.Timestamp;

/**
 * Created by chantrinh on 11/28/15.
 */
public class Threat2 {
    private long id;
    private String distance;
    private Timestamp time;
    private String category;
    private String severity;
    private String incident;
    private String suspect;
    private String latitude;
    private String longitude;

    //empty constructor
    public Threat2(){

    }
    public Threat2(int id, String category, String severity ,String incident, String suspects, String latitude, String longitude, Timestamp time ) {

        this.severity = severity;
        this.category = category;
        this.incident = incident;
        this.suspect = suspects;
        this.latitude = latitude;
        this.longitude = longitude;
        this.id = id;

    }
    public Threat2(String category, String severity, String incident, String suspects, String latitude, String longitude) {

        this.severity = severity;
        this.category = category;
        this.incident = incident;
        this.suspect = suspects;
        this.latitude = latitude;
        this.longitude = longitude;

    }



    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDistance() {
        return distance + " miles away";
    }

    public String getTime() {
        return time + " hours ago";
    }


    public String getIncident() {
        return this.incident;
    }

    public void setIncident(String threat) {
        this.incident = threat;
    }

    public String getSuspect() {
        return this.suspect;
    }

    public void setSuspect(String threat) {
        this.suspect = threat;
    }

    public String getSeverity() {
        return this.severity;
    }

    public void setSeverity(String threat) {
        this.severity = threat;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(String threat) {
        this.category = threat;
    }

    public String getLatitude() {
        return this.latitude;
    }

    public void setLatitude(String threat) {
        this.latitude = threat;
    }

    public String getLongitude() {
        return this.longitude;
    }

    public void setLongitude(String threat) {
        this.longitude = threat;
    }






}
