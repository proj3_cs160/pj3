package com.example.jeffreychien.ttt.ViewThreats;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jeffreychien.ttt.R;
import com.example.jeffreychien.ttt.Threats.Threat;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by jeffreychien on 11/25/15.
 */
public class ViewThreatDetailsActivity extends Activity {
    TextView category, distance, time, severity, numVerifications, description, suspectDescription;

    @Bind(R.id.descriptionHeader) TextView descriptionHeader;
    @Bind(R.id.suspectsHeader) TextView suspectsHeader;

    LinearLayout linearLayout;
    private GoogleMap map;

    // Information to grab from the intent
    private String objectId;
    private boolean verify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_threats_details);
        ButterKnife.bind(this);

        linearLayout = (LinearLayout) findViewById(R.id.topLayout);
        category = (TextView) findViewById(R.id.category);
        distance = (TextView) findViewById(R.id.distance);
        time = (TextView) findViewById(R.id.time);
        severity = (TextView) findViewById(R.id.severity);
        numVerifications = (TextView) findViewById(R.id.numverifications);
        description = (TextView) findViewById(R.id.description);
        suspectDescription = (TextView) findViewById(R.id.suspects);
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();


        Bundle extras = getIntent().getExtras();
        initializeFromBundle(extras);
    }

    public static void start(Context context, String objectId) {
        // Assume no verification unless specified
        start(context, objectId, false);
    }

    public static void start(Context context, String objectId, boolean verify) {
        Intent intent = new Intent(context, ViewThreatDetailsActivity.class);
        intent.putExtra("objectId", objectId);
        intent.putExtra("verify", verify);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onNewIntent (Intent intent) {
        Log.d("test","ON NEW INTENT CALLED " + intent.getExtras().getString("severity"));
        initializeFromBundle(intent.getExtras());
    }

    private void initializeFromBundle(Bundle extras) {
        objectId = extras.getString("objectId");
        verify = extras.getBoolean("verify");
        ParseQuery<Threat> query = ParseQuery.getQuery(Threat.class);
        query.getInBackground(objectId, new GetCallback<Threat>() {
            @Override
            public void done(Threat threat, ParseException e) {
                initializeFromThreat(threat);
            }
        });
    }

    public void initializeFromThreat(Threat threat) {
        // Populate text fields
        category.setText(threat.getCategory());
        distance.setText(threat.milesAway(this, 0, 0)); // TODO fix fallbacking
        time.setText(threat.hoursAgo());
        severity.setText(threat.getSeverity() + " Severity");
        description.setText(threat.getDescription());
        suspectDescription.setText(threat.getSuspectDescription());

        // Hide blank descriptions
        if (threat.getDescription().length() == 0) {
            description.setVisibility(View.GONE);
            descriptionHeader.setVisibility(View.GONE);
        }
        if (threat.getSuspectDescription().length() == 0) {
            suspectDescription.setVisibility(View.GONE);
            suspectsHeader.setVisibility(View.GONE);
        }

        // Handle verification
        int verificationCount = threat.getVotes();
        if (verify) {
            threat.verify();
            Toast.makeText(this, "Verified threat!", Toast.LENGTH_LONG).show();
            verificationCount++;
        }
        numVerifications.setText("Verified by " + verificationCount + " people");

        // Initialize google map
        if (map!=null){
            double latitude = threat.getLatitude();
            double longitude = threat.getLongitude();
            LatLng markerCoordinates = new LatLng(latitude, longitude);
            Marker threatMarker = map.addMarker(new MarkerOptions()
                    .position(markerCoordinates));
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(markerCoordinates, 15));

        }

        // Set background color
        linearLayout.setBackgroundColor(Color.parseColor(threat.getColorHex()));
    }

}
