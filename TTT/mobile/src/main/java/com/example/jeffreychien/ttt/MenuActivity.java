package com.example.jeffreychien.ttt;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.jeffreychien.ttt.ViewThreats.ViewThreatsActivity;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;

public class MenuActivity extends Activity {
    Button addThreatActivityButton, viewThreatsButton, callPoliceButton, deadmansSwitchButton;
    Button addThreatActivityButton2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        // Use full app name in title bar
        getActionBar().setTitle(R.string.app_name);

        final GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();

        addThreatActivityButton = (Button) findViewById(R.id.addThreat);
        addThreatActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this, AddThreatActivity.class));
            }
        });

        /*addThreatActivityButton2 = (Button) findViewById(R.id.addThreat2);
        addThreatActivityButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this, DBActivity.class));
            }
        });*/
        viewThreatsButton = (Button) findViewById(R.id.viewThreats);
        viewThreatsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MenuActivity.this, ViewThreatsActivity.class));
            }
        });
        callPoliceButton = (Button) findViewById(R.id.callPolice);
        callPoliceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + "8434721621"));
                    startActivity(callIntent);
                } catch(SecurityException e) {}
            }
        });

        deadmansSwitchButton = (Button) findViewById(R.id.deadmansSwitch);
        deadmansSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread() {
                    @Override
                    public void run() {
                        MobileUtil.sendMsg(null, mGoogleApiClient, "/deadmans-switch", "msg");
                    }
                }.start();
            }
        });

        Intent threatListenerIntent = new Intent(this, NewThreatListenerService.class);
        startService(threatListenerIntent);
    }

}
