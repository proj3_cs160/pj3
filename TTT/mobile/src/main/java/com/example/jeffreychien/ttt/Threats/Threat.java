package com.example.jeffreychien.ttt.Threats;

import android.app.Activity;
import android.location.Location;
import android.util.Log;

import com.example.jeffreychien.ttt.AddThreatActivity;
import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

@ParseClassName("Threat")
public class Threat extends ParseObject {
    public Threat(){
        // Empty constructor for Parse
    }

    public Double getLatitude() {
        return getDouble("latitude");
    }

    public Double getLongitude() {
        return getDouble("longitude");
    }

    public void setLocation(Location location) {
        put("latitude", location.getLatitude());
        put("longitude", location.getLongitude());
    }

    public String getSeverity() {
        return getString("severity");
    }

    public void setSeverity(String severity) {
        put("severity", severity);
    }

    public String getSuspectDescription() {
        return getString("suspectDescription");
    }

    public void setSuspectDesciption(String suspectDescripiton) {
        put("suspectDescription", suspectDescripiton);
    }

    public String getDescription() {
        return getString("incidentDescription");
    }

    public void setIncidentDescription(String incidentDescription) {
        put("incidentDescription", incidentDescription);
    }

    public String getCategory() {
        return getString("category");
    }

    public void setCategory(String category) {
        put("category", category);
    }

    public String milesAway(double latitude, double longitude) {
        // Calculate distance from threat
        Location here = new Location("");
        here.setLatitude(latitude);
        here.setLongitude(longitude);

        Location there = new Location("");
        there.setLongitude(getLongitude());
        there.setLatitude(getLatitude());
        double distance = here.distanceTo(there);

        // Convert distance from meters to miles
        distance *= 0.000621371;
        return String.format("%.2f", distance) + " miles away";
    }

    public String milesAway(Activity activity, double latitudeFallback, double longitudeFallback) {
        // Set current location from GPS, with a fallback
        Location here = AddThreatActivity.getLocation(activity);
        if (here == null) {
            here = new Location("");
            here.setLatitude(latitudeFallback);
            here.setLongitude(longitudeFallback);
            Log.d("test", "fell back to " + latitudeFallback + "  " + longitudeFallback);
        }

        // Calculate distance from threat
        return milesAway(here.getLatitude(), here.getLongitude());
    }

    public String hoursAgo() {
        // Calculate number of hours since threat was reported
        DateTime created = new DateTime(getCreatedAt());
        int minutes = Minutes.minutesBetween(created, DateTime.now()).getMinutes();
        String hours = String.format("%.2f", minutes / 60.0);
        return hours + " hours ago";
    }

    public String toString(double latitude, double longitude){
        return getCategory() + "|" +
                milesAway(latitude, longitude) + "|" +
                hoursAgo() + "|" +
                getSeverity() + "|" +
                getDescription() + "|" +
                getSuspectDescription() + "|" +
                getObjectId() + "|" +
                getLatitude() + "|" +
                getLongitude();
    }

    public String getColorHex() {
        return Threat.severityToColorHex(getSeverity());
    }

    public static String severityToColorHex(String severity) {
        switch(severity) {
            case "Low":
                return "#ffdd00"; // Yellow
            case "Medium":
                return "#fbb034"; // Orange
            case "High":
            default:
                return "#ff4c4c"; // Red
        }
    }

    public void verify() {
        increment("votes");
        saveInBackground();
    }

    public int getVotes() {
        return getInt("votes");
    }
}
