package com.example.jeffreychien.ttt.Threats.LocalDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chantrinh on 11/24/15.
 */
public class DB extends SQLiteOpenHelper {
    //threat table name
    public static final String TABLE_THREATS = "threats";

    //threat Table Columns names
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_Category = "category";
    public static final String COLUMN_Severity = "severity";
    public static final String COLUMN_Incident = "incident";
    public static final String COLUMN_Suspect = "suspect";
    public static final String COLUMN_Time = "time";
    public static final String COLUMN_Latitude = "latitude";
    public static final String COLUMN_Longitude = "longitude";



    private static final String DATABASE_NAME = "threats.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_THREATS
            + "(" + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_Category + " text,"
            + COLUMN_Severity + " text"
            + COLUMN_Incident + " text"
            + COLUMN_Suspect + " text"
            + COLUMN_Latitude + " real"
            + COLUMN_Longitude + " real"
            +");";

    public DB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    //upgrading db
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DB.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_THREATS);
        onCreate(db);
    }

    // add new threat
    public Threat2 addThreat(Threat2 threat){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_Category, threat.getCategory()); // category
        values.put(COLUMN_Severity, threat.getSeverity()); // severity
        values.put(COLUMN_Incident, threat.getIncident());
        values.put(COLUMN_Suspect, threat.getSuspect());
        values.put(COLUMN_Latitude, threat.getLatitude());
        values.put(COLUMN_Longitude, threat.getLongitude());
        // Inserting Row
        db.insert(TABLE_THREATS, null, values);
        db.close(); // Closing database connection

        return threat;
    }


    // Getting All Threats
    public List<Threat2> getAllThreats() {
        List<Threat2> contactList = new ArrayList<Threat2>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_THREATS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Threat2 threat = new Threat2();
                threat.setId(Integer.parseInt(cursor.getString(0)));
                threat.setCategory(cursor.getString(1));
                threat.setSeverity(cursor.getString(2));
                threat.setLatitude(cursor.getString(3));
                threat.setLongitude(cursor.getString(4));
                // Adding threat to list
                contactList.add(threat);
            } while (cursor.moveToNext());
        }

        // return threat list
        return contactList;
    }

    // Updating single threat
    public int updateThreat(Threat2 threat) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_Category, threat.getCategory());
        values.put(COLUMN_Severity, threat.getSeverity());

        // updating row
        return db.update(TABLE_THREATS, values, COLUMN_ID + " = ?",
                new String[] { String.valueOf(threat.getId()) });
    }

    // Deleting single threat
    public void deleteThreat(Threat2 threat) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_THREATS, COLUMN_ID + " = ?",
                new String[]{String.valueOf(threat.getId())});
        db.close();
    }


    // Getting threats Count
    public int getThreatsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_THREATS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


}
