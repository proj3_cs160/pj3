package com.example.jeffreychien.ttt;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.example.jeffreychien.ttt.ViewThreats.ViewThreatDetailsActivity;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.UnsupportedEncodingException;

/**
 * Created by jeffreychien on 11/25/15.
 */
public class MobileMessageReceiverService extends WearableListenerService {
    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().equals("/threat-details")) {
            try {
                // Get the objectId from the message
                String objectId = new String(messageEvent.getData(), "UTF-8");
                Log.d("test", objectId);

                // Send the objectId to ViewThreatDetailsActivity
                ViewThreatDetailsActivity.start(this, objectId);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else if (messageEvent.getPath().equals("/launch-deadmans-switch")) {
            Intent intent = new Intent(this, DeadmansSwitchConfirmActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);

        } else if (messageEvent.getPath().equals("/call-police")) {
            try {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                callIntent.setData(Uri.parse("tel:" + "8434721621"));
                startActivity(callIntent);
            } catch(SecurityException e) {}
        } else if (messageEvent.getPath().equals("/verify-threat")) {
            try {
                // Get the objectId from the message
                String objectId = new String(messageEvent.getData(), "UTF-8");
                Log.d("test", objectId);

                // Verify on ViewThreatDetailsActivity
                ViewThreatDetailsActivity.start(this, objectId, true);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

    }
}
