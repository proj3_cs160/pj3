package com.example.jeffreychien.ttt.ViewThreats.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeffreychien.ttt.R;
import com.example.jeffreychien.ttt.Threats.Threat;
import com.example.jeffreychien.ttt.ViewThreats.Adapters.RVAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by jeffreychien on 11/20/15.
 */
public class ThreatListViewFragment extends Fragment {
    private List<Threat> threats;
    private RecyclerView rv;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {


        //Inflate the layout for this fragment
        View rootView =  inflater.inflate(
                R.layout.threat_list_view_fragment, container, false);

        rv=(RecyclerView)rootView.findViewById(R.id.rv);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        final Bundle args = getArguments();

        // Get the list of threats starting from most recent
        ParseQuery<Threat> query = ParseQuery.getQuery(Threat.class);
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<Threat>() {
            @Override
            public void done(List<Threat> threats, ParseException e) {
                // Update the adapter
                RVAdapter adapter = new RVAdapter(threats, getActivity(), args.getDouble("latitude"), args.getDouble("longitude"));
                rv.setAdapter(adapter);
            }
        });
    }
}
