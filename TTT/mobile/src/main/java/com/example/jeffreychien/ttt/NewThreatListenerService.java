package com.example.jeffreychien.ttt;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.example.jeffreychien.ttt.Threats.Threat;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.Wearable;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.List;

/**
 * Created by Joeson on 11/30/15.
 */
public class NewThreatListenerService extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String START_ACTIVITY = "/notification";
    private GoogleApiClient mGoogleApiClient;
    private double latitude = 37.8717, longitude = 122.2728;

    private GoogleApiClient getGoogleApiClient(Context context) {
        return new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Wearable.API)
                .build();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public int onStartCommand(Intent intent, int flags, int id) {
        Log.d("got into", "onStartCommand");
        mGoogleApiClient = getGoogleApiClient(this);
        mGoogleApiClient.connect();
        new Thread() {
            @Override
            public void run() {

                while (true) {
                    // Get the list of threats starting from most recent
                    ParseQuery<Threat> query = ParseQuery.getQuery(Threat.class);
                    query.orderByDescending("createdAt");
                    query.findInBackground(new FindCallback<Threat>() {
                        @Override
                        public void done(List<Threat> threats, ParseException e) {
                            if (threats != null) {
                                for (Threat threat : threats) {
                                    // Only report threats within 0.5 miles
                                    float distance = Float.parseFloat(threat.milesAway(latitude, longitude).split(" miles away")[0]);
                                    Log.d("test", "threat " + distance + " miles away detected");
                                    if (distance < 0.5) {
                                        final String msg = threat.toString(latitude, longitude);
                                        Log.d("test", "Sending notification from server : " + msg);
                                        new Thread() {
                                            @Override
                                            public void run() {
                                                MobileUtil.sendMsg(null, mGoogleApiClient, "/notification", msg);
                                            }
                                        }.start();

                                        // Exit after sending one notification to watch
                                        return;
                                    }
                                }
                            }
                        }
                    });

                    //MobileUtil.sendMsg(null, mGoogleApiClient, "/notification", "noise|1.1 miles away|1 hour ago|low|mugging|hoodies");

                    try {
                        // Fetch list every 60 seconds
                        Thread.sleep(60000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
        return START_STICKY;
    }

    @Override
    public void onConnected(Bundle bundle) {
        updateLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        updateLocation();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    private void updateLocation() {
        Location lastLocation =  LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (lastLocation != null) {
            latitude = lastLocation.getLatitude();
            longitude = lastLocation.getLongitude();
        } else {
            latitude = 37.8717;
            longitude = 122.2728;
        }
    }
}
