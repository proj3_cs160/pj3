package com.example.jeffreychien.ttt.ViewThreats.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jeffreychien.ttt.MobileUtil;
import com.example.jeffreychien.ttt.R;
import com.example.jeffreychien.ttt.Threats.Threat;
import com.example.jeffreychien.ttt.ViewThreats.ViewThreatDetailsActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.wearable.Wearable;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.HashMap;
import java.util.List;

/**
 * Created by jeffreychien on 11/20/15.
 */
public class ThreatMapViewFragment extends MapFragment implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    MapView mMapView;
    private GoogleMap googleMap;
    HashMap<Marker, Threat> markerInformation;
    GoogleApiClient mGoogleApiClient;
    private double latitude, longitude;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        markerInformation = new HashMap<>();
        // inflate and return the layout
        View v = inflater.inflate(R.layout.threat_map_view_fragment, container,
                false);
        Bundle args = getArguments();
        latitude = args.getDouble("latitude");
        longitude = args.getDouble("longitude");

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

        mMapView = (MapView) v.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume();// needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        googleMap = mMapView.getMap();
        //Location here = AddThreatActivity.getLocation(getActivity());

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude)).zoom(15).build();
        googleMap.moveCamera(CameraUpdateFactory
                .newCameraPosition(cameraPosition));
        // Instantiates a new CircleOptions object and defines the center and radius
        CircleOptions circleOptions = new CircleOptions()
                .center(new LatLng(latitude, longitude))
                .radius(15).fillColor(Color.parseColor("#00a4e4")).strokeColor(Color.parseColor("#00a4e4")); // In meters

        // Get back the mutable Circle
        Circle circle = googleMap.addCircle(circleOptions);
        // Perform any camera updates here
        ParseQuery<Threat> query = ParseQuery.getQuery(Threat.class);
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<Threat>() {
            @Override
            public void done(List<Threat> threats, ParseException e) {
                for (Threat threat : threats) {
                    Marker threatMarker = googleMap.addMarker(new MarkerOptions()
                            .position(new LatLng(threat.getLatitude(), threat.getLongitude()))
                            .title(threat.getCategory() + " " + threat.hoursAgo())
                            .icon(getMarkerIcon(threat.getColorHex())));
                    markerInformation.put(threatMarker, threat);
                }
            }

        });
        googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                // Show the notification on the watch
                Threat threat = markerInformation.get(marker);
                final String msgData = threat.toString(latitude, longitude);
                new Thread() {
                    @Override
                    public void run() {
                        MobileUtil.sendMsg(null, mGoogleApiClient, "/notification", msgData);
                    }
                }.start();

                // Also bring up view on the phone
                ViewThreatDetailsActivity.start(ThreatMapViewFragment.this.getActivity(), threat.getObjectId());
            }
        });
        return v;
    }

    // Helper method to convert hex string to marker
    // From http://stackoverflow.com/a/33036461
    public BitmapDescriptor getMarkerIcon(String color) {
        float[] hsv = new float[3];
        Color.colorToHSV(Color.parseColor(color), hsv);
        return BitmapDescriptorFactory.defaultMarker(hsv[0]);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(mMapView != null) {
            mMapView.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mMapView != null) {
            mMapView.onPause();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mMapView != null) {
            mMapView.onDestroy();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if(mMapView != null) {
            mMapView.onLowMemory();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
