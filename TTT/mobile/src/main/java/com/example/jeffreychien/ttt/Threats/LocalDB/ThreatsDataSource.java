package com.example.jeffreychien.ttt.Threats.LocalDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.example.jeffreychien.ttt.Threats.LocalDB.DB;
import com.example.jeffreychien.ttt.Threats.LocalDB.Threat2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chantrinh on 11/28/15.
 */
public class ThreatsDataSource {
    // Database fields
    private SQLiteDatabase database;
    private DB dbHelper;
    private String[] allColumns = { DB.COLUMN_ID,
            DB.COLUMN_Category };

    public ThreatsDataSource(Context context) {
        dbHelper = new DB(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Threat2 createThreat(String threat) {
        ContentValues values = new ContentValues();
        values.put(DB.COLUMN_Category, threat);
        long insertId = database.insert(DB.TABLE_THREATS, null,
                values);
        Cursor cursor = database.query(DB.TABLE_THREATS,
                allColumns, DB.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Threat2 newThreat = cursorToThreat(cursor);
        cursor.close();
        return newThreat;
    }

    public void deleteThreat(Threat2 threat) {
        long id = threat.getId();
        System.out.println("Threat deleted with id: " + id);
        database.delete(DB.TABLE_THREATS, DB.COLUMN_ID
                + " = " + id, null);
    }

    public List<Threat2> getAllThreats() {
        List<Threat2> threats = new ArrayList<Threat2>();

        Cursor cursor = database.query(DB.TABLE_THREATS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Threat2 threat = cursorToThreat(cursor);
            threats.add(threat);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return threats;
    }

    private Threat2 cursorToThreat(Cursor cursor) {
        Threat2 threat = new Threat2();
        threat.setId(cursor.getLong(0));
        threat.setCategory(cursor.getString(1));
        return threat;
    }
}
