package com.example.jeffreychien.ttt;

import android.app.Application;

import com.example.jeffreychien.ttt.Threats.Threat;
import com.parse.Parse;
import com.parse.ParseObject;

/**
 * Application remains the same between activities, so it's used to initialize Parse
 */
public class App extends Application {

    @Override public void onCreate() {
        super.onCreate();

        // Register Threat as a custom Parse object
        ParseObject.registerSubclass(Threat.class);

        // Parse API key
        Parse.initialize(
                this,
                "yKTfL719CQJcazpf5VGKLCLleOJj9i0bsGvTsJpl",
                "gs7yiDP3JIOnGA55QJxKP7SNiB41t3cKqhZJ6RZM");

    }
}
