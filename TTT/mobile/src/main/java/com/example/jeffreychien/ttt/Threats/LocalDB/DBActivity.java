package com.example.jeffreychien.ttt.Threats.LocalDB;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.jeffreychien.ttt.R;

import java.util.List;

public class DBActivity extends Activity {
    private DB datasource;
    Spinner categoryChooser, severityChooser;
    private String latitude, longitude;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_db);

        datasource = new DB(this);
        //datasource.open();

        List<Threat2> values = datasource.getAllThreats();

        categoryChooser = (Spinner) findViewById(R.id.categoryChooser1);
        severityChooser = (Spinner) findViewById(R.id.severityChooser1);
        ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter.createFromResource(this,
                R.array.categories, android.R.layout.simple_spinner_item);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categoryChooser.setAdapter(categoryAdapter);

        ArrayAdapter<CharSequence> severityAdapter = ArrayAdapter.createFromResource(this,
                R.array.severities, android.R.layout.simple_spinner_item);
        severityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        severityChooser.setAdapter(severityAdapter);

        // use the SimpleCursorAdapter to show the
        // elements in a ListView
        ArrayAdapter<Threat2> adapter = new ArrayAdapter<Threat2>(this,
                android.R.layout.simple_list_item_1, values);
        //setListAdapter(adapter);
    }

    // Will be called via the onClick attribute
    // of the buttons in main.xml
    public void onClick(View view) {
        @SuppressWarnings("unchecked")

        //ArrayAdapter<Threat2> adapter = (ArrayAdapter<Threat2>) getListAdapter();
        Spinner category = (Spinner) findViewById(R.id.categoryChooser1);
        Spinner severity = (Spinner) findViewById(R.id.severityChooser1);
        EditText SuspectDesc = (EditText) findViewById(R.id.suspects);
        EditText IncidentDesc = (EditText) findViewById(R.id.incident);




        switch (view.getId()) {
            case R.id.add:

                String category1 = category.getSelectedItem().toString();
                String severity1 = severity.getSelectedItem().toString();
                String IncidentDesc1 = String.valueOf((IncidentDesc.getText()).toString());
                String SuspectDesc1 = String.valueOf((SuspectDesc.getText()).toString());
                // save the new threat to the database
                Threat2 threat2 = datasource.addThreat(new Threat2(category1, severity1, IncidentDesc1, SuspectDesc1, latitude="126", longitude="94"));
                //adapter.add(threat2);
                SuspectDesc.setText("");
                IncidentDesc.setText("");
                break;

            /*case R.id.delete:
                if (getListAdapter().getCount() > 0) {
                    threat = (Threat2) getListAdapter().getItem(0);
                    datasource.deleteThreat(threat);
                    adapter.remove(threat);
                }
                break; */
        }
        //adapter.notifyDataSetChanged();
    }

    /*@Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    } */

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }
}
