package com.example.jeffreychien.ttt.ViewThreats.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.jeffreychien.ttt.MobileUtil;
import com.example.jeffreychien.ttt.R;
import com.example.jeffreychien.ttt.Threats.Threat;
import com.example.jeffreychien.ttt.ViewThreats.ViewThreatDetailsActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.wearable.Wearable;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ThreatViewHolder> implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    private GoogleApiClient mGoogleApiClient;
    double latitude, longitude;

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public class ThreatViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView distance;
        TextView time;
        TextView category;
        TextView numVerifications;
        Button severityTagButton;
        public View view;
        public Threat currentItem;

        ThreatViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // Show the notification on the watch
                    Threat threat = currentItem;
                    final String msgData = threat.toString(latitude, longitude);
                    new Thread() {
                        @Override
                        public void run() {
                            MobileUtil.sendMsg(null, mGoogleApiClient, "/notification", msgData);
                        }
                    }.start();

                    // Also bring up view on the phone
                    ViewThreatDetailsActivity.start(view.getContext(), threat.getObjectId());
                }
            });
            cv = (CardView)itemView.findViewById(R.id.cv);
            distance = (TextView) itemView.findViewById(R.id.distance);
            time = (TextView) itemView.findViewById(R.id.time);
            category = (TextView) itemView.findViewById(R.id.category);
            numVerifications = (TextView) itemView.findViewById(R.id.numverifications);
            severityTagButton = (Button) itemView.findViewById(R.id.severityTag);
        }
    }

    List<Threat> threats;
    Activity activity;

    public RVAdapter(List<Threat> threats, Activity activity, double latitude, double longitude){
        this.threats = threats;
        this.activity = activity;
        this.latitude = latitude;
        this.longitude = longitude;

        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public ThreatViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.threat_list_row, viewGroup, false);
        ThreatViewHolder pvh = new ThreatViewHolder(v);
        return pvh;
    }

    @Override
    public void onBindViewHolder(ThreatViewHolder threatViewHolder, int i) {
        Threat threat = threats.get(i);
        threatViewHolder.category.setText(threat.getCategory());
        threatViewHolder.time.setText(threat.hoursAgo());
        threatViewHolder.distance.setText(threat.milesAway(activity, latitude, longitude));
        int numVotes = threat.getVotes();
        threatViewHolder.numVerifications.setText(numVotes + (numVotes == 1 ? " Verification" :" Verifications"));
        threatViewHolder.severityTagButton.setBackgroundColor(Color.parseColor(threat.getColorHex()));
        threatViewHolder.currentItem = threat;
    }

    @Override
    public int getItemCount() {
        return threats.size();
    }

}