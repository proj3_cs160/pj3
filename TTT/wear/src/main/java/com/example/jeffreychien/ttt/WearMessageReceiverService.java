package com.example.jeffreychien.ttt;

import android.content.Intent;
import android.util.Log;

import com.example.jeffreychien.ttt.DeadmansSwitch.DeadmanSwitchMenuActivity;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

import java.io.UnsupportedEncodingException;

/**
 * Created by jeffreychien on 11/24/15.
 */
public class WearMessageReceiverService extends WearableListenerService {
    @Override
    public void onCreate() {
        super.onCreate();
    }
    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        if (messageEvent.getPath().equals("/notification")) {
            try {
                String threatString = new String(messageEvent.getData(), "UTF-8");
                Log.d("test", "RECEIVED MESSAGE" + threatString);
                Intent i = new Intent();
                i.putExtras(WearUtil.decodeThreat(threatString));
                i.setAction("com.example.jeffreychien.ttt.SHOW_NOTIFICATION");
                i.putExtra(MyPostNotificationReceiver.CONTENT_KEY, getString(R.string.title));
                sendBroadcast(i);
            } catch (UnsupportedEncodingException e) {

            }
        } else if (messageEvent.getPath().equals("/deadmans-switch")) {
            Intent i = new Intent(this, DeadmanSwitchMenuActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
        }
    }
}
