package com.example.jeffreychien.ttt;

import android.os.Bundle;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.nio.charset.Charset;

/**
 * Created by jeffreychien on 11/26/15.
 */
public class WearUtil {
    public static void sendMsg(NodeApi.GetConnectedNodesResult nodes, GoogleApiClient googleApiClient, String servicePath, String msg) {
        if (nodes == null) {
            nodes = Wearable.NodeApi.getConnectedNodes(googleApiClient).await();
        }
        for( Node node: nodes.getNodes()) {
            Wearable.MessageApi.sendMessage(
                    googleApiClient, node.getId(),
                    servicePath, msg.getBytes(Charset.forName("UTF-8"))
            ).await();
        }
    }

    public static Bundle decodeThreat(String threatData) {
        // Split threat string into an array, keeping empty values
        // See http://stackoverflow.com/a/14602089
        String[] data = threatData.split("\\|", -1);

        // Build the bundle to return
        Bundle bundle = new Bundle();
        bundle.putString("category", data[0]);
        bundle.putString("distance", data[1]);
        bundle.putString("time", data[2]);
        bundle.putString("severity", data[3]);
        bundle.putString("description", data[4]);
        bundle.putString("suspectdescription", data[5]);
        bundle.putString("objectId", data[6]);
        bundle.putDouble("latitude", Double.parseDouble(data[7]));
        bundle.putDouble("longitude", Double.parseDouble(data[8]));
        return bundle;
    }
}
